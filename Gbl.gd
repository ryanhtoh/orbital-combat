extends Node

# nodes
var ship # user ship
var orbit # user orbit prediction
var ui # user UI
var world # game world
var earth # Starting planet
var design # ship designer unfinished design
var bld # ship finished, valid design

# vars
var zoom = Vector2(1,1)
var px2m = 10000 # m/pix
var s2s = 60*15 # s/s
var spd_conv = px2m / s2s # convert multiplier for pix/s to  m/s
var acc_conv = px2m / pow(s2s, 2)
