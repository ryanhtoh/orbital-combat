extends Node2D

var to_draw = false
sync var to_scuttle = false
sync var scuttle_timer = 15
onready var ship = get_parent()

func _process(delta):
	update()
	if to_scuttle:
		scuttle_timer -= delta
		$scuttle.text = "Scuttle in %.1f" % scuttle_timer
	
	if ship.is_network_master() and scuttle_timer <= 0:
		rpc("end_scuttle", 15)
		ship.rpc("respawn", (2*randf()+1) * Network.radius, randf()*2*PI, false)

func _draw():
	if to_draw:
		draw_line(Vector2.ZERO, ship.linear_velocity/scale.x, Color.aqua, 2)
		draw_line(Vector2.ZERO, ship.accel/scale.x, Color.yellow, 2)

sync func start_scuttle(dummy):
	to_scuttle = true
	$scuttle.show()

sync func end_scuttle(time):
	to_scuttle = false
	scuttle_timer = time
	$scuttle.hide()
