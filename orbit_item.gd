extends "res://grav_item.gd"

var orbit_scene = preload("res://orbit.tscn")
var orbit
var orbit2

func create_orbit(p_id, to_focus):
	var new_orbit = orbit_scene.instance()
	get_parent().call_deferred("add_child", new_orbit)
	new_orbit.calc_orbit(position, linear_velocity, get_parent().G)
	new_orbit.body = self
	set_network_master(p_id)
	self.p_id = p_id
	if is_network_master() and to_focus:
		Gbl.orbit = new_orbit
		Gbl.ship = self
	return new_orbit

sync func delete(id):
	orbit.queue_free()
	if orbit2 != null: orbit2.queue_free()
	queue_free()

sync func reparent(new_parent_path):
	var parent = self.get_parent()
	var new_parent = get_node(new_parent_path)
	var pos_delta = new_parent.position - parent.position
	var vel_delta = new_parent.linear_velocity - parent.linear_velocity
	position -= pos_delta
	linear_velocity -= vel_delta
	parent.remove_child(self)
	parent.remove_child(orbit)
	if orbit2 != null: parent.remove_child(orbit2)
	new_parent.call_deferred("add_child", self)
	new_parent.call_deferred("add_child", orbit)
#	set_owner(new_parent)
	orbit.calc_orbit(position, linear_velocity, new_parent.G)
	if orbit2 != null:
		new_parent.call_deferred("add_child", orbit2)
		orbit2.calc_orbit(position, linear_velocity, new_parent.G)
