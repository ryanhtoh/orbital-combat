extends Label

onready var stats = {
	flow = $Flow.value,
	vel = $Vel.value,
	power = 0,
	cost = 0,
	unit_cost = 2,
	thrust = 0,
	mass_engine = 0,
	unit_mass = 0.3 # GW / tonne
}

signal mass_change(mass)

func _ready():
	recalculate()

func recalculate():
	stats.power = stats.flow * pow(stats.vel, 2) / 2
	stats.cost = stats.power * stats.unit_cost
	stats.thrust = stats.flow * stats.vel
	stats.mass_engine = stats.power *stats. unit_mass
	
	$Flow/Label.text = "Flow: %.2f tonnes/s" % stats.flow
	$Vel/Label.text = "Exhaust Velocity: %d km/s" % stats.vel
	$Stats.text = "Power: %.1f GW\nCost: %d Creds\nThrust: %.2f MN\nEngine Mass: %.1f tonnes" % [stats.power, stats.cost, stats.thrust, stats.mass_engine]

func _on_Flow_value_changed(value):
	stats.flow = value
	recalculate()
	emit_signal("mass_change", stats.mass_engine)
func _on_Vel_value_changed(value):
	stats.vel = value
	recalculate()
	emit_signal("mass_change", stats.mass_engine)

