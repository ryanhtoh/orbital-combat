extends Control

onready var stats = {
	fuel = $Panel/Fuel.value,
	fuel_unit_cost = 1,
	mass1 = 0, # dry mass, tonnes
	mass0 = 0, # wet mass, tonnes
	mass_crew = 100, # tonnes
	mass_armor = 100,
	deltaV = 0,
	acc = 0,
	fuel_cost = 0,
	dry_cost = 0,
	cost = 0,
	prop = $Panel/Prop.stats,
	gen = $Panel/Reactor.stats,
	atk = {
		0: $W1.stats,
		1: $W2.stats,
		2: $W3.stats
	}
}


func _ready():
	Gbl.design = stats
	recalc_mass()
	save()

func recalc_mass():
	stats.mass1 = $W1.stats.mass_sum + $W2.stats.mass_sum + $W3.stats.mass_sum + stats.mass_crew + stats.gen.mass_sum + stats.mass_armor + stats.prop.mass_engine
	stats.mass0 = stats.mass1 + stats.fuel
	stats.deltaV = stats.prop.vel * log(stats.mass0 / stats.mass1)
	stats.acc = stats.prop.thrust * 1000 / stats.mass0
	stats.fuel_cost = stats.fuel * stats.fuel_unit_cost
	stats.dry_cost = $W1.stats.cost_sum + $W2.stats.cost_sum + $W3.stats.cost_sum + stats.prop.cost + stats.gen.cost
	stats.cost = stats.fuel_cost + stats.dry_cost
	$Panel/Fuel/Label.text = "Fuel: %d tonnes" % stats.fuel
	var str1 = "Dry Mass: %.1f t\nFueled Mass: %.1f t\nStarting Acceleration: %.2f m/s2\nDelta V: %d km/s\n\n" % [stats.mass1, stats.mass0, stats.acc, stats.deltaV]
	var str2 = "Fuel Cost: %d Creds \nDry Cost: %d Creds \nTotal Cost: %d Creds" % [stats.fuel_cost, stats.dry_cost, stats.cost]
	$Panel/Fuel/Stats.text = str1 + str2
	
func _on_Save_pressed():
	save()

func save():
	var valid = true # If can afford
	if valid:
		Gbl.bld = stats.duplicate(true)
		print("saved")

func _on_Fuel_value_changed(value):
	stats.fuel = value
	recalc_mass()

func _on_Prop_mass_change(mass):
	recalc_mass()
func _on_Reactor_mass_change(mass):
	recalc_mass()
func _on_Wep_mass_change(mass):
	recalc_mass()

func _on_B1_pressed():
	$W1.show()
	$W2.hide()
	$W3.hide()
func _on_B2_pressed():
	$W1.hide()
	$W2.show()
	$W3.hide()
func _on_B3_pressed():
	$W1.hide()
	$W2.hide()
	$W3.show()

func _on_wep_selected(index, wep_stats, wep_num):
	stats.atk[wep_num] = wep_stats
	print(wep_num)
	recalc_mass()
