extends "res://orbit_item.gd"

var to_send_update = false
sync var last_update = 0

var PFPS = 60
var missile_scene = preload("res://missile.tscn")
var missile_small_scene = preload("res://missile_small.tscn")
var bullet_scene = preload("res://bullet.tscn")
var bullet_small_scene = preload("res://bullet_small.tscn")
sync var max_hp = 100
sync var hp = max_hp

onready var max_energy = Gbl.bld.gen.cap # GJ
onready var energy = max_energy/2 # GJ
onready var energy_regen = Gbl.bld.gen.pwr # GW

var weapon
onready var max_ammo = [Gbl.bld.atk[0].ammo_max, Gbl.bld.atk[1].ammo_max, Gbl.bld.atk[2].ammo_max]
onready var ammo = max_ammo.duplicate(true)

onready var max_fuel = Gbl.bld.fuel
onready var fuel = max_fuel
var dry_mass = Gbl.bld.mass1 # tonnes
var exhaust_vel = Gbl.bld.prop.vel * 1000 / Gbl.spd_conv # converted from m/s
var fuel_flow = Gbl.bld.prop.flow * Gbl.s2s # converted from tonnes/s
var fuel_pwr = 0 # GW
var fuel_sp = Gbl.bld.gen.eff # specific energy, GJ/tonne
var max_fuel_pwr = Gbl.bld.prop.power # GW
var thrust = 0 # converted from m/s2

sync var last_hitter
var bullet_cd = 0
var missile_cd = 0
var mouse_state = 0

var home # for respawning

func _ready():
	type = "ship"
	$rocket_sound.stream_paused = true
	add_to_group("players")

func _process(delta):
	orbit.calc_orbit(position, linear_velocity, get_parent().G)
#	orbit2.calc_orbit(position + Vector2(20,0).rotated(rotation), linear_velocity + Vector2(bullet_speed,0).rotated(rotation), get_parent().G)
	$name.rotation = -rotation
	$name/hp.value = hp
	if Gbl.zoom.x > 3:
		$UI.show()
		$UI.scale = Gbl.zoom * 0.12
	else:
		$UI.hide()

func _unhandled_input(event):
	if is_network_master():
		if event is InputEventKey:
			if Input.is_action_just_pressed("Alt") and is_network_master():
				$name.to_draw = !$name.to_draw
#			if $name.to_draw: orbit2.show()
#			else: orbit2.hide()
		if Input.is_action_pressed("left_click"):
			mouse_state = 1
		elif Input.is_action_pressed("right_click"):
			mouse_state = 2
		elif Input.is_action_pressed("middle_click"):
			mouse_state = 3
		elif Input.is_action_just_released("left_click") and mouse_state == 1:
			mouse_state = 0
		elif Input.is_action_just_released("right_click") and mouse_state == 2:
			mouse_state = 0
		elif Input.is_action_just_released("middle_click") and mouse_state == 3:
			mouse_state = 0

func _physics_process(delta):
	PFPS = 1/delta
	if energy < max_energy:
		energy += delta * energy_regen #* Gbl.s2s
		fuel -= delta * energy_regen / fuel_sp * Gbl.s2s 
	if energy > max_energy:
		energy = max_energy
	if bullet_cd > 0:
		bullet_cd -= delta
	if missile_cd > 0:
		missile_cd -= delta
	var accel = Vector2(0, 0)
	if is_network_master():
		# adjust movement
		look_at( get_global_mouse_position() )
		var thrust_multiplier = Gbl.ui.get_node("thrust").value/100
		var total_mass = fuel + dry_mass
		thrust = (fuel_flow * exhaust_vel / total_mass) * thrust_multiplier
		fuel_pwr = max_fuel_pwr * thrust_multiplier #GW
		if Input.is_action_pressed("thrust") and energy > 0 and fuel > 0:
			$flame_sprite.show()
			$rocket_sound.stream_paused = false
			fuel -= delta * fuel_flow * thrust_multiplier
			linear_velocity += delta * thrust * Vector2(cos(rotation), sin(rotation))
		elif Input.is_action_just_released("thrust"):
			$flame_sprite.hide()
			$rocket_sound.stream_paused = true
		# fire bullets
		if mouse_state > 0:
			fire_wep(mouse_state-1)
		# send update
		if to_send_update:
			rpc_unreliable("sync_vars", position, linear_velocity, rotation, Network.client_clock)
			to_send_update = false
		else:
			to_send_update = true


# Custom functions

func fire_wep(button):
	var wstats = Gbl.bld.atk[button]
	if ammo[button] > 0:
		if Gbl.bld.atk[button].type == "Missile":
			if missile_cd <= 0:
				var missile_speed = wstats.vel * 1000 / Gbl.spd_conv
				var atk_vel = linear_velocity + Vector2(missile_speed, 0).rotated(rotation)
				var dmg = wstats.dmg
				rpc('create_missile', position, atk_vel, dmg)
				missile_cd = wstats.cd
				ammo[button] -= 1
		elif Gbl.bld.atk[button].type == "Railgun":
			var bullet_energy = wstats.energy # GJ
			if bullet_cd <= 0 and energy >= bullet_energy:
				var bullet_speed = wstats.vel * 1000 / Gbl.spd_conv
				var atk_vel = linear_velocity + Vector2(bullet_speed, 0).rotated(rotation)
				var dmg = wstats.dmg
				rpc('create_bullet', position, atk_vel, dmg)
				bullet_cd = wstats.cd
				energy -= bullet_energy
				ammo[button] -= 1

sync func create_bullet(pos, vel, dmg):
	var child
	if dmg > 1:
		child = bullet_scene.instance()
	else:
		child = bullet_small_scene.instance()
	get_parent().add_child(child)
	child.position = pos + Vector2(20,0).rotated(rotation)
	child.rotation = rotation
	child.linear_velocity = vel
	child.p_id = self.p_id 
	child.dmg = dmg
	#child.orbit = child.create_orbit(p_id, false)

sync func create_missile(pos, vel, dmg):
	var child
	if dmg > 20:
		child = missile_scene.instance()
	else: 
		child = missile_small_scene.instance()
	get_parent().add_child(child)
	child.position = pos + Vector2(20,0).rotated(rotation)
	child.rotation = rotation
	child.linear_velocity = vel
	child.p_id = self.p_id
	child.dmg = dmg

sync func damage(damage, damager):
	hp -= damage
	last_hitter = damager
	$dmg.play()

sync func respawn(radius, angle, to_recover):
	$death.play()
	if is_network_master():
		if last_hitter != null:
			Gbl.ui.rpc("increase_score", last_hitter, 1, 0, 0)
		Gbl.ui.rpc("increase_score", p_id, 0, 1, 0)
	if to_recover:
		hp = max_hp
		ammo = max_ammo
		energy = max_energy/2
		fuel = max_fuel
	print(get_parent())
	position = Vector2(radius,0).rotated(angle) - get_parent().position + home.position
	yield(get_tree(), "physics_frame")
	yield(get_tree(), "physics_frame")
	call_deferred("test")
	
func test():
	var speed = sqrt(home.G / position.length())
	linear_velocity = speed * position.normalized().rotated(-PI/2)


sync func spawn(p_id, radius, angle):
	home = Gbl.earth
	position = Vector2(radius,0).rotated(angle)
	var speed = sqrt(get_parent().G / position.length())
	linear_velocity = speed * position.normalized().rotated(-PI/2)
	rpc("sync_vars", position, linear_velocity, rotation, Network.client_clock)
	orbit = create_orbit(p_id, true)
#	orbit2 = create_orbit(p_id, false)
	if is_network_master():
		$name/name_txt.add_color_override("font_color", Color(0.5, 1, 0.5))
		orbit.color = Color.green
		orbit.z_index = -1
		$name.to_draw = true
#		orbit2.show()
	else:
		orbit.z_index = -2
		$name.to_draw = false
		if Network.teams_on:
			if Network.teams[p_id] == Network.teams[Network.player_id]:
				$name/name_txt.add_color_override("font_color", Color(0.5, 0.5, 1))
				orbit.color = Color.blue
			else:
				$name/name_txt.add_color_override("font_color", Color(1, 0.5, 0.5))
				orbit.color = Color.red
		else:
			$name/name_txt.add_color_override("font_color", Color(1, 0.5, 0.5))
			orbit.color = Color.red
	orbit.nb_points = 400
#	orbit2.color = Color.orangered
#	orbit2.nb_points = 400
#	orbit2.get_node("forecast").show_t = false

sync func sync_vars(pos, vel, rot, time):
	if time > last_update:
		last_update = time
		position = pos
		linear_velocity = vel
		rotation = rot
