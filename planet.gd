extends Area2D

var p_id = 0
var type = "planet"
var linear_velocity = Vector2.ZERO
var orbital_layer = 1
var G = 0.4 * float("3.98600e14") * pow(Gbl.s2s, 2) / pow(Gbl.px2m, 3)
const BOUNDS = 3500

func _ready():
	Gbl.earth = self
	update()

func _draw():
	draw_arc(Vector2.ZERO, BOUNDS, 0, 2*PI, 100, Color.orange, 0.5)

func _physics_process(delta):
	$Sprite.rotation -= delta*0.043633

func _on_planet_area_entered(area):
	if area.is_network_master() and area.get("type") != null:
		if area.type == "bullet" or area.type == "nuke":
			area.rpc("delete", 0)

func _on_Center_area_entered(area):
	if area.is_network_master() and area.get("type") != null:
		if area.type == "ship":
			area.rpc("respawn", (randf()+1) * Network.radius, randf()*2*PI, true)

func reparent_child(child, new_parent):
	remove_child(child)
	new_parent.add_child(child)
