extends CanvasLayer

sync var player_labels = {}
sync var player_scores = {}
sync var team_scores = {1:{name="Team", score=0, kills=0, deaths=0, dmg=0}, 2:{name="Team", score=0, kills=0, deaths=0, dmg=0}}
var mont = preload("res://montserrat_font.tres")

func _ready():
	Gbl.ui = self

func _process(_delta):
	var ship = Gbl.ship
	var orbit = Gbl.orbit
	$energy.max_value = ship.max_energy
	$energy.value = ship.energy
	$energy/Label.text = "%.1f MJ / %.1f MJ" % [ship.energy, ship.max_energy]
	$hp.value = ship.hp
	$hp/Label.text = "%d HP / %d HP" % [ship.hp, ship.max_hp]
	$fuel.max_value = ship.max_fuel
	$fuel.value = ship.fuel
	$fuel/Label.text = "%.1f kt / %.1f kt" % [ship.fuel/1000, ship.max_fuel/1000]
	var forecast_t = $forecast.value
	$forecast/Label.text = "Forecast: %.1f sec (%d min in-game)" % [forecast_t, forecast_t * Gbl.s2s/60]
	$thrust/Label.text = "Thrust: %.2f m/s2 (%.2f GW)" % [ship.thrust*Gbl.acc_conv, ship.fuel_pwr]
	$railgun/Label.text = "Railgun Speed %.2f m/s (%.2f J)" % [$railgun.value, $railgun.value]
	if ship != null and orbit.e != null:
		var str1 = "FPS: %.0f, PFPS: %.0f, Ping: %d ms, OI: %d \n" % [Engine.get_frames_per_second(), ship.PFPS, Network.ping, orbit.iterations]
		var str2 = "Semi-minor Axis: %.1f Mm \n" % [orbit.b/100]
		var str3 = "Semi-major Axis: %.1f Mm \nEccentricity: %.2f \n" % [orbit.a/100, orbit.ecc]
		var str4 = "Orbital Radius: %.1f Mm \nSpeed: %.2f km/s \n" % [ship.position.length()/100, ship.linear_velocity.length()*10/Gbl.s2s]
		var str5 = "Mean Anomaly: %.2f \nEccentric Anomaly: %.2f" % [orbit.M, orbit.E]
		var str6 = "\nGravity: %.2f m/s2 \nDir: %d" % [ship.accel.length()*Gbl.acc_conv, orbit.vel_dir]
		$Text.text = str1 + str2 + str3 + str4 + str5 + str6
	var str1 = "HP: \nEnergy: \nFuel: \n"
	var ammo_str1 = "Ammo: %d \n" % [ship.ammo[0]]
	var ammo_str2 = "Ammo: %d \n" % [ship.ammo[1]]
	var ammo_str3 = "Ammo: %d \n" % [ship.ammo[2]]
	$Stat_txt.text = str1 + ammo_str1 + ammo_str2 + ammo_str3

func _unhandled_input(event):
	if Input.is_action_just_pressed("tab"):
		if Network.teams_on:
			$Team_Scores.visible = true
		else:
			$Scores.visible = true
	elif Input.is_action_just_released("tab"):
		$Scores.visible = false
		$Team_Scores.visible = false

func _on_Respawn_pressed():
	Gbl.ship.get_node("name").rpc("start_scuttle", 0)

sync func increase_score(for_who, k, d, dmg):
	assert(for_who in player_scores)
	var pl = player_scores[for_who]
	pl.kills += k
	pl.deaths += d
	pl.dmg += dmg
	pl.score += k*100 + dmg - d*100
	if Network.teams_on:
		var team = Network.teams[for_who]
		team_scores[team].kills += k
		team_scores[team].deaths += d
		team_scores[team].dmg += dmg
		team_scores[team].score += k*100 + dmg - d*100
	refresh_scores()

func refresh_scores():
	delete_grid()
	for grid in range(4):
		add_cell("", grid)
		add_cell("Score", grid)
		add_cell("Kills", grid)
		add_cell("Deaths", grid)
		add_cell("Damage", grid)
	if Network.teams_on:
		for team in Network.team_groups:
			var grid
			if Network.teams[Network.player_id] == 1:
				grid = team+1
			else:
				grid = 4-team
			add_team(team, grid)
			Network.team_groups[team].sort_custom(self, "sort_score")
			for player in Network.team_groups[team]:
				add_player(player, grid)
	else:
		Network.team_groups[1] = player_scores.keys()
		Network.team_groups[1].sort_custom(self, "sort_score")
		for player in Network.team_groups[1]:
			add_player(player, 1)

func add_player(id, grid):
	add_cell(player_scores[id].name, grid)
	add_cell(str(player_scores[id].score), grid)
	add_cell(str(player_scores[id].kills), grid)
	add_cell(str(player_scores[id].deaths), grid)
	add_cell(str(player_scores[id].dmg), grid)

func add_team(id, grid):
	add_cell(team_scores[id].name, grid)
	add_cell(str(team_scores[id].score), grid)
	add_cell(str(team_scores[id].kills), grid)
	add_cell(str(team_scores[id].deaths), grid)
	add_cell(str(team_scores[id].dmg), grid)

func add_cell(text, grid):
	var label = Label.new()
	label.set_align(Label.ALIGN_CENTER)
	label.set_text(text)
	label.set_h_size_flags(Control.SIZE_EXPAND_FILL)
	label.add_font_override("font", mont)
	if grid == 1:
		$Scores/Grid.add_child(label)
	elif grid == 2:
		$Team_Scores/Grid1.add_child(label)
	elif grid == 3:
		$Team_Scores/Grid2.add_child(label)

func delete_grid():
	for cell in $Scores/Grid.get_children():
		cell.queue_free()
	for cell in $Team_Scores/Grid1.get_children():
		cell.queue_free()
	for cell in $Team_Scores/Grid2.get_children():
		cell.queue_free()

func sort_score(a, b):
	if player_scores[a].score > player_scores[b].score:
		return true
	return false
