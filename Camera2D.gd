extends Camera2D

onready var overlay = get_parent().get_node("name")
const zoom_amt = 1.3
const start_zoom = 2.5
const max_zoom = 2.5
const min_zoom = 1
var map = false

func _ready():
	set_zoom(Vector2(start_zoom, start_zoom))

func _input(event):
	if event.is_pressed() and event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_UP && get_zoom().x > min_zoom and not map:
			set_zoom(get_zoom()/zoom_amt)
			Gbl.zoom = get_zoom()
			overlay.scale = get_zoom()
		elif event.button_index == BUTTON_WHEEL_DOWN && get_zoom().x < max_zoom and not map:
			set_zoom(get_zoom()*zoom_amt)
			Gbl.zoom = get_zoom()
			overlay.scale = get_zoom()

func _unhandled_input(event):
	if is_network_master():
		if event is InputEventKey:
			if Input.is_action_just_pressed("M"):
				if map:
					set_zoom(Gbl.zoom)
					map = false
				else:
					set_zoom(Vector2(10,10))
					map = true
				overlay.scale = get_zoom()
