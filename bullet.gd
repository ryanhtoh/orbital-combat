extends "res://grav_item.gd"

var timer = 0
var dmg = 20

func _ready():
	type = "bullet"
	$fire.play()

func _physics_process(delta):
	timer += delta
	if timer > 20 and is_network_master():
		rpc("delete", 0)

func _on_bullet_entered(body):
	if "p_id" in body and body.p_id != p_id and is_network_master():
		if body.is_in_group("players"):
			damage(dmg, body)
