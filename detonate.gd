extends Area2D

var type = "detonate"
const POINT_COUNT = 40

# Called when the node enters the scene tree for the first time.
func _ready():
	update()

func _draw():
	draw_arc(Vector2.ZERO, $Collision.shape.radius, 0, 2*PI, POINT_COUNT, Color.red, 1)
