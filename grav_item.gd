extends Area2D

var linear_velocity = Vector2.ZERO
var type
var p_id
var accel = Vector2.ZERO

func _process(delta):
	if Gbl.zoom.x > 3:
		$UI.show()
		if type == "bullet":
			$UI.scale = Gbl.zoom * Vector2(0.09, 0.05)
		elif type == "nuke":
			$UI.scale = Gbl.zoom * 0.12
	else:
		$UI.hide()

func _physics_process(delta):
	interpolate_forward(delta)

func interpolate_forward(delta):
	accel = -get_parent().G * position.normalized() / position.length_squared()
	linear_velocity += accel*delta
	position += linear_velocity*delta

func interpolate_backward(delta):
	position -= linear_velocity*delta
	accel = -get_parent().G * position.normalized() / position.length_squared()
	linear_velocity -= accel*delta
	

sync func delete(id):
	queue_free()

func damage(dmg, body):
	if Network.teams_on:
		if Network.teams[body.p_id] != Network.teams[p_id]:
			Gbl.ui.rpc("increase_score", p_id, 0, 0, dmg)
	else:
		Gbl.ui.rpc("increase_score", p_id, 0, 0, dmg)
	body.rpc("damage", dmg, p_id)
	if body.hp <= 0:
		body.rpc("respawn", (2*randf()+1) * Network.radius, randf()*2*PI, true)
	if type == "bullet":
		rpc("delete", 0)
