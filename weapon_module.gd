extends Control

export var label = "Weapon 1"
export var start_type = 0

onready var stats = {
	type = "Railgun",
	# Masses
	gun_mass = 0,
	unit_mass = 1,
	mass_sum = 0,
	# Costs
	unit_cost = 0,
	gun_cost = 0,
	cost_sum = 0,
	# Stats
	ammo = 0,
	ammo_max = 0,
	vel = 0,
	energy = 0,
	dmg = 0,
	cd = 0
}

signal mass_change(mass)
signal wep_selected(index, stats)

func _ready():
	$Label.text = label
	$List.add_item("Heavy Railgun")
	$List.add_item("Nuclear Missile")
	$List.add_item("Point Defense Railgun")
	$List.add_item("Medium Railgun")
	$List.add_item("Interceptor Missile")
	$List.select(start_type)
	swap_wep(start_type)

func swap_wep(wep_type):
	var text = $List.get_item_text(wep_type) + ":\n"
	for node in get_children():
		if node.is_in_group("wep"):
			node.hide()
	match $List.get_item_text(wep_type):
		"Heavy Railgun":
			stats.type = "Railgun"
			stats.vel = 10
			stats.energy = 50
			stats.cd = 0.6
			stats.dmg = 20
			stats.ammo = 200
			stats.ammo_max = 200
			text += "This deals moderate damage at long range."
		"Nuclear Missile":
			stats.type = "Missile"
			stats.vel = 7
			stats.energy = 10
			stats.cd = 0.5
			stats.dmg = 100
			stats.ammo = 14
			stats.ammo_max = 14
			text += "Nukes can destroy ships or other nukes. Can be destroyed by bullets."
		"Point Defense Railgun":
			stats.type = "Railgun"
			stats.vel = 5
			stats.energy = 10
			stats.cd = 0.25
			stats.dmg = 1
			stats.ammo = 500
			stats.ammo_max = 500
			text += "Rapid-fire CIWS system, good for shooting down nukes. Barely penetrates ship armor."
		"Medium Railgun":
			stats.type = "Railgun"
			stats.vel = 7.1
			stats.energy = 25
			stats.cd = 0.4
			stats.dmg = 10
			stats.ammo = 300
			stats.ammo_max = 300
			text += "Multi-role mid-range railgun. Good against both nukes and other ships."
		"Interceptor Missile":
			stats.type = "Missile"
			stats.vel = 7
			stats.energy = 10
			stats.cd = 0.4
			stats.dmg = 20
			stats.ammo = 30
			stats.ammo_max = 30
			text += "Small, cheap missile designed to destroy other missles. Deals less damage to ships. Can be destroyed by bullets."
	text += "\n\n"
	text += "Energy: %d MJ \n" % stats.energy
	text += "Velocity: %.1f \n" % stats.vel
	text += "Cooldown: %.2f sec \n" % stats.cd
	text += "Damage: %d hp \n" % stats.dmg
	text += "DPS: %.2f hp/sec \n" % [stats.dmg / stats.cd]
	text += "Max Power Draw: %.2f MJ/sec \n" % [stats.energy / stats.cd]
	$Panel/Label.text = text

func _on_wep_mass_change(mass):
	emit_signal("mass_change", mass)

func _on_List_item_selected(index):
	swap_wep(index)
	emit_signal("wep_selected", index, stats)
	get_parent().save()
