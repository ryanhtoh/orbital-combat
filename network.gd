extends Node

var player_scene = preload("res://player_ship.tscn")
var item_scene = preload("res://item.tscn")

# Default game port. Can be any number between 1024 and 49151.
const DEFAULT_PORT = 6007 #10567

# Max number of players.
const MAX_PEERS = 16
const SEPARATION = 600

# Name for my player.
var player_name = ""
var player_id
var time_start = 0.0
var timer = 0
var ping = -1
var client_clock = 0

# Names for remote players in id:name format.
var players = {}
sync var teams_on
sync var teams = {}
sync var team_groups = {1:[], 2:[]}
var players_ready = []
var radius
var spawn_angle

# Signals to let lobby GUI know what's going on.
signal player_list_changed()
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)

# Callback from SceneTree.
func _player_connected(id):
	# Registration of a client beings here, tell the connected player that we are here.
	rpc_id(id, "register_player", player_name, teams, teams_on, team_groups)

# Callback from SceneTree.
func _player_disconnected(id):
	if has_node("/root/World"): # Game is in progress.
		if get_tree().is_network_server():
			emit_signal("game_error", "Player " + players[id] + " disconnected")
			end_game()
	else: # Game is not in progress.
		# Unregister this player.
		unregister_player(id)

# Callback from SceneTree, only for clients (not server).
func _connected_ok():
	# We just connected to a server
	emit_signal("connection_succeeded")

# Callback from SceneTree, only for clients (not server).
func _server_disconnected():
	emit_signal("game_error", "Server disconnected")
	end_game()

# Callback from SceneTree, only for clients (not server).
func _connected_fail():
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")


# Lobby management functions.
# Obtained from other players when player first joins
remote func register_player(new_player_name, teams, teams_on, team_groups):
	var id = get_tree().get_rpc_sender_id()
	players[id] = new_player_name
	if id == 1: # copy settings if host
		self.teams_on = teams_on
		self.teams = teams
		self.team_groups = team_groups
		if teams_on:
			get_tree().get_root().get_node("Lobby/Teams").show()
	emit_signal("player_list_changed")

func unregister_player(id):
	players.erase(id)
	teams.erase(id)
	for team in team_groups:
		team_groups[team].erase(id)
	emit_signal("player_list_changed")

remote func pre_start_game(spawn_points):
	# Change scene.
	var num = spawn_points.size()
	radius = SEPARATION*num/(2*PI)
	if radius < 600: radius = 600
	var world = load("res://world.tscn").instance()
	get_tree().get_root().add_child(world)
	var lobby = get_tree().get_root().get_node("Lobby")
	lobby.hide()
	get_tree().get_root().remove_child(lobby)
	world.get_node("UI").add_child(lobby)
	
	var angle_sep = 2*PI/num
	spawn_angle = 0
	for i in range(10):
		var item = item_scene.instance()
		world.get_node("items").add_child(item)
		item.item_type = "fuel"
		item.init()
	for i in range(3):
		var item = item_scene.instance()
		world.get_node("items").add_child(item)
		item.item_type = "hp"
		item.init()
	for i in range(3):
		var item = item_scene.instance()
		world.get_node("items").add_child(item)
		item.item_type = "energy"
		item.init()
	for i in range(3):
		var item = item_scene.instance()
		world.get_node("items").add_child(item)
		item.item_type = "nuke"
		item.init()
	for p_id in spawn_points:
		var player = player_scene.instance()
		world.get_node("Earth").add_child(player)
		player.set_name(str(p_id)) # Use unique ID as node name.
		spawn_angle += angle_sep
		player.spawn(p_id, radius, spawn_angle)
		
		player_id = get_tree().get_network_unique_id()
		if p_id == player_id:
			if teams_on:
				player.get_node("name/name_txt").text = player_name + ", Team: " + str(teams[p_id]) # If node for this peer id, set name.
			else:
				player.get_node("name/name_txt").text = player_name
			player.get_node("camera").current = true
			world.get_node("UI").player_scores[p_id] = {name=player_name, score=0, kills=0, deaths=0, dmg=0}
		else:
			if teams_on:
				player.get_node("name/name_txt").text = players[p_id] + ", Team: " + str(teams[p_id]) # If node for this peer id, set name.
			else:
				player.get_node("name/name_txt").text = players[p_id]
			world.get_node("UI").player_scores[p_id] = {name=players[p_id], score=0, kills=0, deaths=0, dmg=0}
	world.get_node("UI").refresh_scores()
	time_start = OS.get_ticks_msec()
	if not get_tree().is_network_server():
		# Tell server we are ready to start.
		rpc_id(1, "ready_to_start", player_id)
	elif players.size() == 0:
		post_start_game()

remote func post_start_game():
	if get_tree().get_rpc_sender_id() == 1:
		get_tree().set_pause(false) # Unpause and unleash the game!

remote func ready_to_start(id):
	assert(get_tree().is_network_server())
	if not id in players_ready:
		players_ready.append(id)
	if players_ready.size() == players.size():
		for p in players:
			rpc_id(p, "post_start_game")
		post_start_game()

func host_game(new_player_name):
	player_name = new_player_name
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(host)

func join_game(ip, new_player_name):
	player_name = new_player_name
	var client = NetworkedMultiplayerENet.new()
	client.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(client)

func begin_game():
	assert(get_tree().is_network_server())
	# Create a dictionary with peer id and respective spawn points, could be improved by randomizing.
	var spawn_points = {}
	spawn_points[1] = 0 # Server in spawn point 0.
	var spawn_point_idx = 1
	for p in players:
		spawn_points[p] = spawn_point_idx
		spawn_point_idx += 1
	# Call to pre-start game with the spawn points.
	for p in players:
		rpc_id(p, "pre_start_game", spawn_points)
	pre_start_game(spawn_points)

func end_game():
	if has_node("/root/world"): # Game is in progress.
		get_node("/root/world").queue_free()
	emit_signal("game_ended")
	players.clear()

func _ready():
	player_id = get_tree().get_network_unique_id()
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
func _physics_process(delta):
	client_clock += int(delta*1000)
	timer += delta
	var wait_time = 0.2
	if timer >= wait_time and time_start > 0:
		timer -= wait_time
		rpc_id(1, "reflect_ping", OS.get_ticks_msec())

sync func reflect_ping(client_time):
	var p_id = get_tree().get_rpc_sender_id()
	rpc_id(p_id, "finish_ping", OS.get_ticks_msec(), client_time)

sync func finish_ping(server_time, client_time):
	ping = OS.get_ticks_msec() - client_time
	client_clock = server_time + ping/2
