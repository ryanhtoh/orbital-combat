extends "res://orbit_item.gd"

var orbital_layer = 2
var G = float("4.90487e12") * pow(Gbl.s2s, 2) / pow(Gbl.px2m, 3)
export var Gmult = 2.0

func _ready():
	G *= Gmult
	$sprite.scale = Vector2(0.1, 0.1) * sqrt(Gmult)
	$CollisionShape2D.shape.radius = 26 * sqrt(Gmult)
	p_id = 0
	type = "planet"
	update()
	spawn(p_id, position.length(), PI)

func _draw():
	var r_soi = position.length() * pow(G/get_parent().G, 0.4)
	draw_arc(Vector2.ZERO, r_soi, 0, 2*PI, 100, Color.gray, 0.5)
	$SOI/Shape.scale = Vector2(r_soi/1000, r_soi/1000)

func spawn(p_id, radius, angle):
#	position = Vector2(radius,0).rotated(angle)
	var speed = sqrt(get_parent().G / position.length())
	linear_velocity = speed * position.normalized().rotated(-PI/2)
	orbit = create_orbit(p_id, false)
	orbit.color = Color.gray
	orbit.nb_points = 400
	orbit.calc_orbit(position, linear_velocity, get_parent().G)

func _on_SOI_area_entered(area):
	if area.is_network_master() and area.get("type") != null:
		if area.type == "shipCenter" and area.timer_running == false:
			var ship = area.get_parent()
			if ship.get_parent().orbital_layer < orbital_layer:
				ship.rpc("reparent", ship.get_path_to(self))
#				area.get_node("Timer").start()
				area.timer_running = true
		if area.type == "nuke":
			if area.get_parent().orbital_layer < orbital_layer:
				area.rpc("reparent", area.get_path_to(self))

func _on_SOI_area_exited(area):
	if area.is_network_master() and area.get("type") != null:
		if area.type == "shipCenter" and area.timer_running == false:
			var ship = area.get_parent()
			if ship.get_parent().orbital_layer == orbital_layer:
				ship.rpc("reparent", ship.get_path_to(get_parent()))
#				area.get_node("Timer").start()
				area.timer_running = true
		if area.type == "nuke":
			if area.get_parent().orbital_layer == orbital_layer:
				area.rpc("reparent", area.get_path_to(get_parent()))

func _on_area_entered(area):
	if area.is_network_master() and area.get("type") != null:
		if area.type == "bullet" or area.type == "nuke":
			area.rpc("delete", 0)

func _on_Center_area_entered(area):
	if area.is_network_master() and area.get("type") != null:
		if area.type == "ship":
			if area.get_parent().orbital_layer == orbital_layer:
				area.rpc("respawn", (randf()+1) * Network.radius, randf()*2*PI, true)

func reparent_child(child, new_parent):
	remove_child(child)
	new_parent.add_child(child)
