extends Control

var G = float("0.01") * pow(Gbl.s2s, 2) / pow(Gbl.px2m, 3)
var linear_velocity = Vector2.ZERO

func _ready():
	Gbl.world = self
	randomize()


func _on_Bounds_area_exited(area):
	if area.is_network_master() and area.get("type") != null:
		if area.type == "ship" and area.position.length() >= 3500:
			area.rpc("respawn", (randf()+1) * Network.radius, randf()*2*PI, true)
