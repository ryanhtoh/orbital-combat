extends Node2D

sync var color = Color.red
sync var size = 1
var nb_points = 200
# Orbital params
var points_arc = PoolVector2Array()
var a # semi-major axis
var b # semi-minor axis
var e # eccentricity vector
var h # specific angular momentum
var f_half = Vector2.ZERO # foci
var ecc = 0.0 # eccentricity
var body
var E = 0# eccentric anomaly (angle from center)
var M = 0# mean anomaly (time)
var vel_dir = 0
var vel = Vector2.ZERO
var iterations = 0 # Iterations of Newton's Method last used for prediction

func _draw():
	for index_point in range(points_arc.size()-1):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color, size)

func _process(delta):
	look_at(2*f_half + get_parent().global_position)
	if $forecast.show_t:
		predict_pos(Gbl.ui.get_node("forecast").value)
		$forecast.show()
		$forecast.update()
	else:
		$forecast.hide()

func calc_orbit(pos, vel, mu):
	h = pos[0]*vel[1] - pos[1]*vel[0] # specific angular momentum
	var r = pos.length()
	a = mu*r / ( 2*mu - r*vel.length_squared() ) # semi-major axis length
	e = Vector2(pos[0]/r - h*vel[1]/mu, pos[1]/r + h*vel[0]/mu) # eccentricity vector
	f_half = a*e # empty focus location
	ecc = e.length() # eccentricity
	points_arc = PoolVector2Array()
	if ecc <= 1: # elliptical orbit
		b = sqrt(pow(a, 2) - f_half.length_squared()) # semi-minor axis length
		for i in range(nb_points + 1):
			var eccentric_anomaly = 2*PI*i/nb_points # angle from apsis
			var next_pos = Vector2(a*cos(eccentric_anomaly), b*sin(eccentric_anomaly))
			points_arc.push_back(next_pos)
	else: # hyberbolic orbit
		b = a * sqrt(pow(ecc, 2) - 1)
		for i in range(-nb_points, nb_points + 1):
			var eccentric_anomaly = 2*PI*i/nb_points
			var next_pos = Vector2(-a*(ecc-cosh(eccentric_anomaly)), b*sinh(eccentric_anomaly))
			points_arc.push_back(next_pos)
	if ecc <= 1: position = f_half
	else: position = Vector2(0,0)
	update()

# Predicts future position after time t
func predict_pos(t):
	var mu = get_parent().G
	var pos = to_local(body.global_position)
	vel = body.linear_velocity.rotated(-rotation)
	var M1 # Predicted time
	var E1 # Predicted angle
	var diff = true # continue newtons
	iterations = 0 # newtons iterations
	if ecc <= 1:
		get_vel_dir(pos, false)
		if pos.y > 0:
			E = PI - acos(pos.x / a)*vel_dir
		else:
			E = PI + acos(pos.x / a)*vel_dir
		M = E - ecc * sin(E) # mean anomaly (time)
		M1 = M + t * sqrt(mu/pow(a,3)) # predicted time
		E1 = newtons_method(M1, M1, false)
		while diff:
			iterations += 1
			var e_prev = E1
			E1 = newtons_method(E1, M1, false)
			if abs(e_prev - E1) < 0.06 or iterations > 40: diff = false
		E1 = PI - E1*vel_dir
		if iterations <= 40: $forecast.point = Vector2(a*cos(E1), b*sin(E1))
		else: $forecast.point = pos + vel*t
	else:
		get_vel_dir(pos, true)
		var l = -b*b/a # semi-latus rectum
		var r = body.position.length()
		var true_anom = acos((l/r-1) / ecc)
		var coshE = (cos(true_anom) + ecc)/(1+ecc*cos(true_anom))
		if pos.y > 0:
			E = -log(coshE + sqrt(coshE*coshE - 1))
		else:
			E = log(coshE + sqrt(coshE*coshE - 1))
		M = ecc * sinh(E) - E
		M1 = M - t*vel_dir * sqrt(mu/pow(-a,3)) # predicted time
		E1 = newtons_method(M1, M1, true)
		while diff:
			iterations += 1
			var e_prev = E1
			E1 = newtons_method(E1, M1, true)
			if abs(e_prev - E1) < 0.06 or iterations > 50: diff = false
		if iterations <= 50: $forecast.point = Vector2(-a*(ecc-cosh(E1)), b*sinh(E1))
		else: $forecast.point = pos + vel*t

func newtons_method(E, M, is_hyperbolic):
	if is_hyperbolic:
		var F = ecc*sinh(E) - E - M
		var Fprime = ecc*cosh(E) - 1
		return E - F/Fprime
	else:
		return E - (E-ecc*sin(E)-M) / (1-ecc*cos(E))

# Gets whether orbit is clockwise or ccw
func get_vel_dir(pos, is_hyperbolic):
	if is_hyperbolic:
		if vel.y > 0: vel_dir = 1
		elif vel.y <0: vel_dir = -1
	else:
		if pos.y > 0:
			if vel.x > 0: vel_dir = 1
			elif vel.x < 0: vel_dir = -1
		elif pos.y < 0:
			if vel.x > 0: vel_dir = -1
			elif vel.x < 0: vel_dir = 1
