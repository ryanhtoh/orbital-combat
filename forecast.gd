extends Node2D

var show_t = true
var point = Vector2.ZERO

func _draw():
	if show_t: draw_circle(point, 4*Gbl.zoom.x, get_parent().color)
