extends Control

func _ready():
	Network.connect("connection_failed", self, "_on_connection_failed")
	Network.connect("connection_succeeded", self, "_on_connection_success")
	Network.connect("player_list_changed", self, "refresh_lobby")
	Network.connect("game_ended", self, "_on_game_ended")
	Network.connect("game_error", self, "_on_game_error")
	# Set the player name according to the system username. Fallback to the path.
	if OS.has_environment("USERNAME"):
		$Connect/Name.text = OS.get_environment("USERNAME")
	else:
		var desktop_path = OS.get_system_dir(0).replace("\\", "/").split("/")
		$Connect/Name.text = desktop_path[desktop_path.size() - 2]

func _on_host_pressed():
	if $Connect/Name.text == "":
		$Connect/ErrorLabel.text = "Invalid name!"
		return
	$Connect.hide()
	$Players.show()
	$GameMode.show()
	$Connect/ErrorLabel.text = ""
	var player_name = $Connect/Name.text
	Network.host_game(player_name)
	refresh_lobby()

func _on_join_pressed():
	if $Connect/Name.text == "":
		$Connect/ErrorLabel.text = "Invalid name!"
		return
	var ip = "127.0.0.1" #get_node("Connect/IP").text
	if not ip.is_valid_ip_address():
		$Connect/ErrorLabel.text = "Invalid IP address!"
		return
	$Connect/ErrorLabel.text = ""
	$Connect/Host.disabled = true
	$Connect/Join.disabled = true
	var player_name = $Connect/Name.text
	Network.join_game(ip, player_name)

func _on_connection_success():
	$Connect.hide()
	$Players.show()

func _on_connection_failed():
	$Connect/Host.disabled = false
	$Connect/Join.disabled = false
	$Connect/ErrorLabel.set_text("Connection failed.")

func _on_game_ended():
	show()
	$Connect.show()
	$Players.hide()
	$Connect/Host.disabled = false
	$Connect/Join.disabled = false

func _on_game_error(errtxt):
	$ErrorDialog.dialog_text = errtxt
	$ErrorDialog.popup_centered_minsize()
	$Connect/Host.disabled = false
	$Connect/Join.disabled = false

func refresh_lobby():
	$Players/List.clear()
	var str1 = ""
	var player_id = get_tree().get_network_unique_id()
	if Network.teams.has(player_id):
		str1 = " (Team " + str(Network.teams[player_id]) + ")"
	$Players/List.add_item(Network.player_name + " (You)" + str1)
	for p in Network.players:
		var str2 = ""
		if Network.teams.has(p):
			str2 = " (Team " + str(Network.teams[p]) + ")"
		$Players/List.add_item(Network.players[p] + str2)
	$Players/Start.disabled = not get_tree().is_network_server()

func _on_start_pressed():
	Network.begin_game()


func _on_DM_pressed():
	rpc("select_mode", "DM")

func _on_TeamDM_pressed():
	rpc("select_mode", "TDM")


func _on_1_pressed():
	rpc("set_team", 1)

func _on_2_pressed():
	rpc("set_team", 2)

sync func select_mode(type):
	if type == "DM":
		$GameMode/Label.text = "Deathmatch"
		rpc("reset_teams", false)
	if type == "TDM":
		$GameMode/Label.text = "Team Deathmatch"
		rpc("reset_teams", true)
	
sync func set_team(team):
	var id = get_tree().get_rpc_sender_id()
	Network.teams[id] = team
	Network.team_groups[team].append(id)
	Network.team_groups[3-team].erase(id)
	refresh_lobby()

sync func reset_teams(teams_on):
	if teams_on:
		Network.teams_on = true
		$Teams.show()
	else:
		Network.teams_on = false
		Network.teams = {}
		Network.team_groups = {1:[], 2:[]}
		$Teams.hide()
		refresh_lobby()
