extends Area2D

var type = "item"
export (String) var item_type = "fuel"
var img_nuke = preload("res://assets/rocket_item.png")
var img_energy = preload("res://assets/battery-pack.png")
var img_hp = preload("res://assets/heart-plus.png")
var img_fuel = preload("res://assets/gas-pump.png")
const BUFFER_RAD = 300
const RADIUS = 2500

sync func used(angle_seed, distance_seed):
	var distance = distance_seed * RADIUS + BUFFER_RAD
	position = Vector2(distance, 0).rotated(angle_seed*2*PI)

func _on_rocket_item_area_entered(area):
	if area.is_network_master() and area.get("type") != null and area.type == "ship":
		if item_type == "nuke":
			for i in range(3):
				area.ammo[i] += area.max_ammo[i]/4
				if area.ammo[i] > area.max_ammo[i]:
					area.ammo[i] = area.max_ammo[i]
			rpc("used", randf(), randf())
		elif item_type == "energy" and area.energy < area.max_energy:
			area.energy += 100
			if area.energy > area.max_energy:
				area.rset("energy", area.max_energy)
			rpc("used", randf(), randf())
		elif item_type == "hp" and area.hp < area.max_hp:
			area.hp += 100
			if area.hp > area.max_hp:
				area.rset("hp", area.max_hp)
			rpc("used", randf(), randf())
		elif item_type == "fuel" and area.fuel < area.max_fuel:
			area.fuel = area.max_fuel
			rpc("used", randf(), randf())

func _ready():
	init()

func init():
	if item_type == "nuke": $Sprite.texture = img_nuke
	elif item_type == "energy": $Sprite.texture = img_energy
	elif item_type == "hp": $Sprite.texture = img_hp
	elif item_type == "fuel": $Sprite.texture = img_fuel
	if is_network_master():
		rpc("used", randf(), randf())
