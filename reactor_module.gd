extends Label

onready var stats = {
	pwr = $Pwr.value, # GW
	eff = $Eff.value, # TJ/tonne
	cap = $Cap.value, # GJ
	cost = 0,
	reactor_unit_cost = 40,
	cap_unit_cost = 2,
	cap_mass = 0,
	reactor_mass = 0,
	mass_sum = 0,
	reactor_density = 30,
	cap_density = 1/2.54
}

signal mass_change(mass)

func _ready():
	recalculate()

func recalculate():
	stats.cap_mass = stats.cap * stats.cap_density 
	stats.reactor_mass = stats.pwr * stats.reactor_density * (2 + stats.eff/1000)
	stats.mass_sum = stats.cap_mass + stats.reactor_mass
	stats.cost = stats.reactor_mass * stats.reactor_unit_cost + stats.cap_mass * stats.cap_unit_cost
	$Pwr/Label.text = "Power: %.2f GW (%.1f GJ per IRL sec)" % [stats.pwr, stats.pwr*Gbl.s2s]
	$Eff/Label.text = "Efficiency: %d GJ/t fuel" % stats.eff
	$Cap/Label.text = "Capacitors: %d GJ (%d t)" % [stats.cap, stats.cap_mass]
	var str1 = "Reactor Mass: %.1f t\nTotal Mass: %.1f t\n" % [stats.reactor_mass, stats.mass_sum]
	var str2 = "Cost %d Creds" % stats.cost
	$Stats.text = str1+str2

func _on_Pwr_value_changed(value):
	stats.pwr = value
	recalculate()
	emit_signal("mass_change", stats.mass_sum)
func _on_Eff_value_changed(value):
	stats.eff = value
	recalculate()
	emit_signal("mass_change", stats.mass_sum)
func _on_Cap_value_changed(value):
	stats.cap = value
	recalculate()
	emit_signal("mass_change", stats.mass_sum)
