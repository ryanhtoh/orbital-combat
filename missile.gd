extends "res://grav_item.gd"

var timer = 0
var dmg = 100
var detonated = false

func _ready():
	type = "nuke"
	$fire.play()

func _physics_process(delta):
	timer += delta
	if timer > 20 and is_network_master():
		rpc("delete", 0)

func _on_detonate_area_entered(body):
	if detonated == false and "p_id" in body and body.p_id != p_id and is_network_master():
		if body.is_in_group("players"):
			rpc("boom_anim", 0)
			damage(dmg, body)
			detonated = true
		elif body.type == "nuke":
			rpc("boom_anim", 0)
			detonated = true

func _on_area_entered(body):
	if "p_id" in body and body.p_id != p_id and is_network_master():
		if body.type == "bullet":
			body.rpc("delete", 0)
			rpc("delete", 0)

func _on_explosion_animation_finished():
	rpc("delete", 0)
	
sync func boom_anim(dummy):
	$explosion.play()
	$detonate.hide()
	$rocket_sprite.hide()
